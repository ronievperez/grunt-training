module.exports = function(grunt) {
    var mozjpeg = require('imagemin-mozjpeg');
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
        options: {
            'no-write': true
          },
          public: 'dist/**/*'
        },

        // scss compile
        sass: {
            options: {
              loadPath: ['src/scss', 'bower_components/foundation-sites/scss'],
            },
            dist: {
              options: { sourcemap: 'none' },
              files: { 'dist/css/app.css': 'src/scss/app.scss' }
            }
        },

        cssmin: {
            css: {
                src: 'dist/css/app.css',
                dest: 'dist/css/app.min.css'
            }
        },

        jshint: {
            files: ['Gruntfile.js', 'dist/js/*.js'],
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },

        concat: {
          options: { separator: ';' },
          script: {
            src: [          
              'bower_components/jquery/dist/jquery.js'
            ],
            dest: 'dist/js/main.js'
          }
        },

        uglify: {
          all: {
            files: {
              'dist/js/main.min.js' : [ 'dist/js/main.js' ]
            }
          }
        },

        imagemin: {                          // Task
            static: {                          // Target
              options: {                       // Target options
                optimizationLevel: 3,
                svgoPlugins: [{ removeViewBox: false }],
                use: [mozjpeg()]
              },
              files: {                         // Dictionary of files
                'dist/images/dog.jpg': 'src/images/dog.jpg', // 'destination': 'source'
              }
            }
        },

        
        watch: {
            options: { livereload: true },
            grunt: {
                files: ['gruntfile.js'],
                tasks: ['default']
            },
          sass: {
            files: 'src/scss/**/*.scss',
            tasks: ['build-css']
          },
          html: {
            files: ['./**.html'],
          }
        }
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Register your grunt task(s).
    grunt.registerTask('build-css', ['sass', 'cssmin']);
    grunt.registerTask('build-js', ['concat', 'uglify']);
    grunt.registerTask('minify', ['newer:uglify:all']);
    grunt.registerTask('compile', ['clean', 'imagemin']);

    // Default task(s).
    grunt.registerTask('default', ['build-css', 'build-js', 'minify', 'compile', 'watch']);

};
